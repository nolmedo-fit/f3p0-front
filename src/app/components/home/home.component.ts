import { Component, OnInit } from '@angular/core';
import { faClock, faTasks, faTools, faEnvelope, faCogs } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  faClock = faClock;
  faTasks = faTasks;
  faTools = faTools;
  faEnvelope = faEnvelope;
  faCogs = faCogs;
  public username = 'Nico';

  constructor() { }

  ngOnInit(): void {
  }

}
