import { Component, OnInit } from '@angular/core';
import { faHouseUser, faUserFriends, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  fahouse = faHouseUser;
  fauser = faUserFriends;
  faexit = faSignOutAlt;
  constructor() { }

  ngOnInit(): void {
  }

}
