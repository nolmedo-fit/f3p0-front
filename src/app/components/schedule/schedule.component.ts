import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})

export class ScheduleComponent implements OnInit {


  constructor() { }

    filterCollab= '';

    collaborators = [
      {
        "name": "acartechini",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "cgimenez",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "eaccerboni",
        "hours": 6,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "fescobar",
        "hours": 6,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "fnery",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "ftamburro",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "ftarasco",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "gbide",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "spinat",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "gmoriello",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "nolmedo",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "vmiranda",
        "hours": 6,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "rcorsiglia",
        "hours": 6,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "pstumpf",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "mgonzalez",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "emartinez",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "jperez",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "jlopez",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "msolei",
        "hours": 6,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      },
      {
        "name": "szark",
        "hours": 8,
        "weekone": 0.0,
        "weektwo": 0.0,
        "weekthree": 0.0,
        "weekfour": 0.0,
        "weekfive":0.0
      }
    ];

  ngOnInit(): void {
  }

}
