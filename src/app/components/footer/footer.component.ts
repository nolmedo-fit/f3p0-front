import { Component, OnInit } from '@angular/core';
import { faMapMarkerAlt, faPhoneSquareAlt, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import {faFacebook, faTwitter, faLinkedinIn} from '@fortawesome/free-brands-svg-icons'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }
  
  faMapMarkerAlt = faMapMarkerAlt;
  faPhoneSquareAlt = faPhoneSquareAlt;
  faEnvelope = faEnvelope;
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faLinkedinIn = faLinkedinIn;

  ngOnInit(): void {
  }

}
